# ESLint config for TypeScript

A shareable [ESLint](https://eslint.org/) config for TypeScript projects.

## Installation

Run `npm i -D icode-eslint-config` to install this package.

Also run following command to install its required dependencies

`npm i -D eslint @typescript-eslint/eslint-plugin`

### Make compatible with prettier

Run `npm i -D eslint-config-prettier` to install this package.

### Configure eslint to run on commit

Run `npm i -D husky lint-staged` to install this package.

After running this command, add this to package.json to enable eslint on commit

    "scripts": {
    	"lint": "eslint \"e2e/**/*.{js,ts}\" \"src/**/*.{js,ts}\" \"*.js\""
    },
    "husky": {
    	"hooks": {
    		"pre-commit": "lint-staged"
    	}
    },
    "lint-staged": {
    	"*.{js,ts}": [
    		"prettier --write",
    		"eslint",
    		"git add"
    	]
    }

`prettier --write` if you are using prettier

### .eslintrc.js file example

    'use strict';

    module.exports = {
    	extends: ['icode-eslint-config', 'prettier', 'prettier/@typescript-eslint'],
    	parserOptions: {
    		project: './tsconfig.json'
    	},
    	rules: {
    		// Project-specific overrides.

    		// For Angular forwardRef.
    		'@typescript-eslint/no-use-before-define': ['error', { classes: false }],

    		'@typescript-eslint/interface-name-prefix': 'error',
    		'@typescript-eslint/no-explicit-any': 'error'
    	}
    };

## Development

Run `npm install` to install the project dependencies.

## Unused rules

### Has issues

- `@typescript-eslint/no-extraneous-class`

- https://github.com/typescript-eslint/typescript-eslint/issues/812

- `@typescript-eslint/no-floating-promises`

- https://github.com/typescript-eslint/typescript-eslint/issues/815

- `@typescript-eslint/no-unnecessary-condition`

- https://github.com/typescript-eslint/typescript-eslint/issues/996

- https://github.com/typescript-eslint/typescript-eslint/issues/1015

- `@typescript-eslint/strict-boolean-expressions`

- https://github.com/typescript-eslint/typescript-eslint/issues/754

- `@typescript-eslint/unbound-method`

- https://github.com/typescript-eslint/typescript-eslint/issues/636

- https://github.com/typescript-eslint/typescript-eslint/issues/700

- https://github.com/typescript-eslint/typescript-eslint/issues/743

### Too strict

- `@typescript-eslint/no-extra-parens` – extra parentheses sometimes aid readability.

- `@typescript-eslint/no-magic-numbers` – difficult to follow, especially in existing projects.

- `@typescript-eslint/no-type-alias` – partly handled by `@typescript-eslint/consistent-type-definitions`.

- `@typescript-eslint/prefer-readonly` – team decision.

### Handled by TypeScript

- `@typescript-eslint/no-unused-vars` – `noUnusedLocals` and `noUnusedParameters`.

- `@typescript-eslint/typedef` – `noImplicitAny`.

- `getter-return` – ts(2378).

- `no-dupe-args` – ts(2300).

- `no-dupe-keys` – ts(1117).

- `no-func-assign` – ts(2539).

- `no-import-assign` – ts(2539).

- `no-unreachable` – ts(7027).

- `valid-typeof` – ts(2367).

### Unnecessary

- `no-async-promise-executor` – unnecessary with `@typescript-eslint/no-misused-promises`.

## Used rules with issues

- `@typescript-eslint/ban-types`

- https://github.com/typescript-eslint/typescript-eslint/issues/842

- `@typescript-eslint/member-naming`

- https://github.com/typescript-eslint/typescript-eslint/issues/816

- `@typescript-eslint/member-ordering`

- https://github.com/typescript-eslint/typescript-eslint/issues/395

- https://github.com/typescript-eslint/typescript-eslint/issues/929
